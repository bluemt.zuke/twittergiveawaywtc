# 請照說明申請Twitter API權限，並填妥下面資訊
API_KEY = ''
API_SECRET = ''
BEARER_TOKEN = ''
ACCESS_TOKEN = ''
ACCESS_SECRET = ''

# 在這裡加上你常常參加抽獎的朋友twitter帳號(前面不加@)，
# 當這個用戶有Retweet抽獎的時候，你也跟著他Retweet+Link+Follow+Tag Friends
# * 建議數量不要太多，容易被Twitter封鎖 *
# Example: CHECK_USERS_RETWEET = ['TwitterName1', 'TwitterName2', 'TwitterName3']
CHECK_USERS_RETWEET = []

# 在這裡加上你想參加抽獎的項目方Twitter帳號(前面不加@)，
# 當這些帳號有發出Giveaway貼文的時候，你會對文章做Retweet+Link+Follow+Tag Friends
# * 建議數量不要太多，容易被Twitter封鎖 *
GIVEAWAY_TWITTERS = ['cryptowilson_', 'aiden_janice', 'AlphaBeesClub']


# 檢查你的Twitter list中的文章，
# 當這些帳號有發出Giveaway貼文或有Retweet抽獎的時候，你會對文章做Retweet+Link+Follow+Tag Friends
# 可用以取代CHECK_USERS_RETWEET
# * 建議數量不要太多，容易被Twitter封鎖 *
# Example: CHECK_TWITTER_LIST = [1539645754321123456, 15396450000000003456]
CHECK_TWITTER_LIST = []


# 加上你要tag的朋友清單，請多輸入一點(一般最少三位，建議輸入五位)，若朋友數目不夠就不會參加抽獎。
FRIENDS = [
    "A", "B", "C", "D", "E",
]

# 抽獎的關鍵字判斷，文章中出現這些字會被當成抽獎。
# 沒特殊需求可以不用修改
GIVEAWAY_KEYWORDS = ['giveaway', 'giving away', 'give away', 'whitelist', 'white list', "WL", '抽', "白名單", "白單"]

# 第一次執行時，尋找多久時間以內的tweet，60為60分鐘
# 建議不要太長，短時間參加太多抽獎會被Twitter封鎖
CHECK_RECENT_TWEET = 60
