import importlib
from datetime import datetime, timedelta
import tweepy
import json
import random
import re
import os
import time
import twitter_config
import pytz


# =========================
# 以下不需修改
LOG = False
CHECK_INTERVAL = 30  # 檢查的頻率，30為30分鐘
RETWEET_INTERVAL = 60  # 每次Retweet的時間間隔。一分鐘一推為比較安全的時間。
USER_CACHE = 'tweet_users.txt'
JOINED_GIVEAWAY = "tweet_joined.txt"
joined_list = []
last_check_time = None
user_id_map = None

try:
    twitter_config.CHECK_TWITTER_LIST
except AttributeError:
    setattr(twitter_config, 'CHECK_TWITTER_LIST', [])

def printlog(msg):
    now = datetime.now()
    msg = f'{now} - {msg}'
    print(msg)
    filename = f'log_{now.strftime("%Y%m%d")}.txt'
    with open(filename, 'a+', encoding='utf-8') as f2:
        f2.write(f'{msg}\n')


def get_user_id(client, user_name):
    global user_id_map
    user_name = user_name.lower()
    if user_name in user_id_map:
        return user_id_map[user_name]
    resp = client.get_user(username=user_name)
    if not resp.data:
        return None
    else:
        user_id_map[resp.data.username.lower()] = resp.data.id
        json.dump(user_id_map, open(USER_CACHE, "w+"))
        return resp.data.id


def check_giveaway(tweet_list):
    giveaway_tweets = []
    for t in tweet_list:
        tweet_text = re.sub('@([A-Za-z0-9_]{1,15})|#([A-Za-z0-9_]{1,15})|http[^\t\n,]*', '', t.text.lower())
        if str(t.id) not in get_joined_list() and any(
                k.lower() in tweet_text for k in twitter_config.GIVEAWAY_KEYWORDS):
            giveaway_tweets.append(t)
    return giveaway_tweets


def get_list_tweet_giveaways(client, twitter_list_id):
    tweets = []
    next_token = None
    resp = None
    while resp is None or next_token is not None:
        # Search limit rate 1 second
        time.sleep(1)
        if LOG:
            printlog(f"  ::get_list_tweets:{twitter_list_id}")
        resp = client.get_list_tweets(twitter_list_id, tweet_fields=['author_id', 'referenced_tweets', "created_at"],
                                      max_results=70, pagination_token=next_token)
        if not resp.data:
            break
        next_token = resp.meta.get('next_token')
        for t in resp.data:
            if t.created_at < pytz.UTC.localize(last_check_time):
                next_token = None
                break
            tweets.append(t)
    return handle_tweets(client, tweets)


def get_user_retweet_giveaways(client, twitter_name_list):
    names = " OR from:".join(twitter_name_list)
    query = f'(from:{names}) is:retweet'
    tweets = search_tweets(client, query)
    tweets = handle_tweets(client, tweets)
    return check_giveaway(tweets)


def get_twitter_giveaways(client, twitter_name_list):
    names = " OR @".join(twitter_name_list)
    query = f'(@{names}) -is:retweet -is:reply'
    tweets = search_tweets(client, query)
    tweets = [t for t in tweets if not t.referenced_tweets]
    return check_giveaway(tweets)


def search_tweets(client, query):
    result = []
    next_token = None
    resp = None
    while resp is None or next_token is not None:
        # Search limit rate 1 second
        time.sleep(1)
        if LOG:
            printlog(f"  ::search:{query}")
        resp = client.search_recent_tweets(
            query, tweet_fields=['author_id', 'referenced_tweets'], max_results=70, start_time=last_check_time,
            next_token=next_token)
        if not resp.data:
            break
        next_token = resp.meta.get('next_token')
        result.extend(resp.data)
    return result


def handle_tweets(client, tweets):
    if not tweets:
        return []
    result = []
    retweet_id_list = []
    for t in tweets:
        if t.referenced_tweets:
            for r in t.referenced_tweets:
                if r.type == "retweeted":
                    retweet_id_list.append(r.id)
        else:
            result.extend(check_giveaway(tweets))

    while retweet_id_list:
        # Sleep to prevent limit rate
        time.sleep(1)
        if LOG:
            printlog(f"  ::get_tweets:{retweet_id_list[:100]}")
        resp = client.get_tweets(ids=retweet_id_list[:100], tweet_fields=['author_id'])
        # Do not retweet the retweet tweet.
        exclude_reply_list = [t for t in resp.data if not t.referenced_tweets]
        result.extend(check_giveaway(exclude_reply_list))
        retweet_id_list = retweet_id_list[100:]
    return result


def get_joined_list():
    global joined_list
    if not joined_list:
        with open(JOINED_GIVEAWAY, 'r') as f2:
            joined_list = f2.read().splitlines()
    return joined_list


def do_giveaway(client, tweet):
    # Exit if already do giveaway
    if str(tweet.id) in get_joined_list():
        return
    try:
        time.sleep(RETWEET_INTERVAL * 0.6)
        # RT + Like + Follow
        if LOG:
            printlog(f"  ::retweet:{tweet.id}")
        client.retweet(tweet.id)
        time.sleep(RETWEET_INTERVAL * 0.2)
        if LOG:
            printlog(f"  ::like:{tweet.id}")
        client.like(tweet.id)
        time.sleep(RETWEET_INTERVAL * 0.2)

        matcher = re.search('(follow|关注|關注|跟隨)((.|\n){,100})\n', tweet.text, re.IGNORECASE)
        if matcher:
            need_follow = set(re.findall("@([A-Za-z0-9_]{1,15})", matcher.group(2)))
        else:
            need_follow = set(re.findall("@([A-Za-z0-9_]{1,15})", tweet.text))
        for un in need_follow:
            if un in user_id_map:
                continue
            uid = get_user_id(client, un)
            if not uid:
                continue
            if LOG:
                printlog(f"  ::follow:{uid}")
            client.follow(uid)
            time.sleep(3)
        # Reply & Tag
        tag_count = 3
        matcher = re.search("(tag|@ |mention).*(more|\\+|entry)", tweet.text, re.IGNORECASE)
        if matcher:
            tag_count = len(twitter_config.FRIENDS)
        else:
            matcher = re.search("(tag|@ |mention).*(\\d+).*", tweet.text, re.IGNORECASE)
            if matcher:
                tag_count = int(matcher.group(2))
            elif any(k in tweet.text.lower() for k in ["tag", "@ ", "mention"]):
                tag_count = 3

        if tag_count > 0:
            random.shuffle(twitter_config.FRIENDS)
            reply_text = "\n@" + f"\n@".join(twitter_config.FRIENDS[0:tag_count])
            if LOG:
                printlog(f"  ::reply:{tweet.id}")
            client.create_tweet(text=reply_text, in_reply_to_tweet_id=tweet.id)

        with open(JOINED_GIVEAWAY, 'a+') as f2:
            f2.write(f'{tweet.id}\n')
        joined_list.append(str(tweet.id))
    finally:
        printlog(f"Join Giveaway: https://twitter.com/{tweet.author_id}/status/{tweet.id}")
        printlog("    " + tweet.text.split('\n', 1)[0])


def main():
    client = tweepy.Client(bearer_token=twitter_config.BEARER_TOKEN,
                           consumer_key=twitter_config.API_KEY,
                           consumer_secret=twitter_config.API_SECRET,
                           access_token=twitter_config.ACCESS_TOKEN,
                           access_token_secret=twitter_config.ACCESS_SECRET,
                           wait_on_rate_limit=True)
    giveaway_tweets = []
    for list_id in twitter_config.CHECK_TWITTER_LIST:
        giveaway_tweets.extend(get_list_tweet_giveaways(client, list_id))

    if twitter_config.CHECK_USERS_RETWEET:
        i = 0
        while i < len(twitter_config.CHECK_USERS_RETWEET):
            giveaway_tweets.extend(get_user_retweet_giveaways(client, twitter_config.CHECK_USERS_RETWEET[i:i + 20]))
            i += 20

    if twitter_config.GIVEAWAY_TWITTERS:
        i = 0
        while i < len(twitter_config.GIVEAWAY_TWITTERS):
            giveaway_tweets.extend(get_twitter_giveaways(client, twitter_config.GIVEAWAY_TWITTERS[i:i + 20]))
            i += 20

    printlog(f'Found {len(giveaway_tweets)} giveaways')
    for t in giveaway_tweets:
        do_giveaway(client, t)


if __name__ == '__main__':
    if not os.path.exists(USER_CACHE):
        with open(USER_CACHE, 'a+') as f:
            f.write("{}")
    user_id_map = json.load(open(USER_CACHE))

    if not os.path.exists(JOINED_GIVEAWAY):
        with open(JOINED_GIVEAWAY, 'a') as f:
            pass

    last_check_time = datetime.utcnow() - timedelta(minutes=twitter_config.CHECK_RECENT_TWEET)
    while True:
        check_begin_time = datetime.utcnow()
        printlog(f"> Check twitter begin. last {int((check_begin_time - last_check_time).total_seconds() / 60)} minutes events.")
        try:
            importlib.reload(twitter_config)
            main()
            last_check_time = check_begin_time
        except Exception as e:
            printlog(str(e))
            printlog(str(e.args))
            last_check_time -= timedelta(seconds=1)
        wait_seconds = (check_begin_time.timestamp() + CHECK_INTERVAL * 60) - datetime.utcnow().timestamp()
        printlog(f"< Check twitter finish. Wait for {int(wait_seconds/60)} minutes.")
        if wait_seconds > 0:
            time.sleep(wait_seconds)
